import React from 'react';
import { connect } from 'react-redux';
import { addTodo } from '../actions';

const AddTodo = ({ dispatch }) => {
  let input;
  return(
    <form
      onSubmit={(e) => {
        e.preventDefault();
        if (input.value !== '') {
          dispatch(addTodo(input.value));
        }
        
        input.value = '';}
      }  
      className='add_todo'>
      <input
        ref={node => {input = node}} 
        className='form__control m-b-md'
        placeholder='What do you need to-do?'
        />
      <button 
        className="btn btn-success btn-sm"
        type='submit'
      >
      Add todo
      </button>
    </form>
  );
};

export default connect()(AddTodo);