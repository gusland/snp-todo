import { connect } from 'react-redux';
import { toggleTodo, deleteTodo, toggleAll } from '../actions';
import TodoList from './TodoList';

const getVisibleTodos = (
  todos,
  filter
) => {
  switch (filter) {
    case 'SHOW_ALL':
      return todos;
    case 'SHOW_COMPLETED':
      return todos.filter(
        t => t.completed
      );
    case 'SHOW_ACTIVE':
      return todos.filter(
        t => !t.completed
      );
  }
}

const mapStateToTodoListProps = (state) => ({
  todos: getVisibleTodos(state.todos, state.visibilityFilter),
});

const mapDispatchToTodoListProps = (dispatch) => ({
  onToggleAllClick: (e) => {
    dispatch(toggleAll(e.target.checked));
  },
  onToggleClick: (id) => {
    dispatch(toggleTodo(id));
  },
  onRemoveClick: (id) => {
    dispatch(deleteTodo(id));
  }
});

const VisibleTodoList = connect(
  mapStateToTodoListProps,
  mapDispatchToTodoListProps
)(TodoList);

export default VisibleTodoList;