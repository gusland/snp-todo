import React from 'react';

const Todo = ({
  onToggleClick,
  onRemoveClick,
  completed,
  text
}) => (
<tr>
  <td
    className={ completed ? 'completed' : 'active'}
  >
    {text}
  </td>
  <td>
    <div className='pull-right'>
      <button 
        onClick={onToggleClick} 
        className="btn btn-default btn-sm"
      >
      { completed ? 'Uncomplete ' : 'Complete'}
      

      </button>
      {'  '}
      <button 
        onClick={onRemoveClick} 
        className="btn btn-danger btn-sm">
        delete
      </button>
    </div>
  </td>
</tr>
);

export default Todo;