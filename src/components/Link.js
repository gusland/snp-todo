import React from 'react';

const Link = ({active,children,onClick,}) => {
  if (active) {
    return <span className='btn active'> {children} </span>
  }
  return (
    <a href ='#'
      className='btn btn-link'
      onClick={e => {
        e.preventDefault();
        onClick();
      }}
    >
      {children}
    </a>
  );
};


export default Link;