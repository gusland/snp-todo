import React from 'react';
import Todo from './Todo';


const TodoList = ({
  todos,
  onToggleClick,
  onToggleAllClick,
  onRemoveClick
}) => (
<div className='todo_list_container'>
  
  <table className='todos'>
    <thead>
      <tr>
        <td>
          <div className='toggle_all'>
            <input 
              id="toggle_all" 
              onChange={(e) => onToggleAllClick(e)}
              type="checkbox"
            />
            {/* <label className='toggle_all' htmlFor="toggle_all">Mark all as complete</label> */}
          </div>
        </td>
      </tr>
    </thead>
    <tbody>
     
      {todos.map(todo => 
        <Todo
          key={todo.id}
          {...todo}
          onToggleClick={() => onToggleClick(todo.id)} 
          onRemoveClick={() => onRemoveClick(todo.id)}
          /> 
      )}
    </tbody>
  </table>
</div>
);

export default TodoList;