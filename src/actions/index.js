import {v4} from 'node-uuid';

export const addTodo = (text) => ({
  type: 'ADD_TODO',
  id: v4(),
  text,
});

export const deleteTodo = (id) => {
  return {
    type: 'DELETE_TODO',
    id,
  }
}

export const setVisibilityFilter = (filter) => ({
  type: 'SET_VISIBILITY_FILTER',
  filter,
});


export const toggleTodo = (id) => ({
  type: 'TOGGLE_TODO',
  id
});

export const toggleAll = (checked) => ({
  type : 'TOGGLE_ALL',
  checked,
});
    
